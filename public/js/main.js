$(document).ready(function () {
    $(".gallery").slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
]
    });
    $(".slick-prev").append("<i class=\"fas fa-angle-left\"></i>");
});